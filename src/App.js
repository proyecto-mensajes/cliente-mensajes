import React from 'react';
import './App.css';

import {BrowserRouter as Router, Route} from 'react-router-dom';

import Admin from './components/Admin'

import MsgForm from './components/MsgForm';

function App() {
  return (
    <Router>
      <Route path="/" exact component={Admin}/>
      <Route path="/mensajes" component={MsgForm}/>
    </Router>
    );
}

export default App;
