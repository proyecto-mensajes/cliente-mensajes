import React, { useState, useEffect } from 'react';
import { Button, Form, Grid, Header, Segment } from 'semantic-ui-react';

const MsgForm = () => {
  const [nombre, setNombre] = useState('');
  const [mensaje, setMensaje] = useState('');
  const [data, setData] = useState({});

  const payload = () => {
      setData({
        nombre,
        mensaje
      })
  }

  useEffect(() => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'default', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', 
        body: JSON.stringify(data)
    };
    fetch('http://localhost:8000/usuario/', requestOptions)
        .then(response => response.json())
        .then(data => data)
        .catch(() => {
          console.log('Internal server error');
        });
    }, [data]);
    
          
    return (
    <>
      <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' color='teal' textAlign='center'>
          Contactanos
          </Header>
          <Form size='large' method="post" onSubmit={(event) => event.preventDefault}>
            <Segment stacked>
              <Form.Input fluid icon='user' iconPosition='left' placeholder='Escribe aquí tu nombre' onChange={(event) => setNombre(event.target.value) }/>
              <textarea placeholder="Envía aquì tu mensaje" rows="2" onChange={(event) => setMensaje(event.target.value)}></textarea>
              <Button className="button-top" color='teal' fluid size='large' onClick={payload}>
                Enviar Mensaje
              </Button>
            </Segment>
          </Form>
          {/* {mostrar ? <MsgExitoso></MsgExitoso> : null}  */}
        </Grid.Column>
      </Grid>
    
    </>  
)

}

export default MsgForm