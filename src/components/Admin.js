import React from 'react'
import { Link } from 'react-router-dom'


import {
    Button,
    Container,
    Header,
    Segment,
  } from 'semantic-ui-react'

const style = {
    h1: {
      marginTop: '3em',
    },
    h2: {
      margin: '4em 0em 2em',
    },
    h3: {
      marginTop: '2em',
      padding: '2em 0em',
    },
    last: {
      marginBottom: '300px',
    },
  }
  
const Admin = () => {
   return (
       <>
        <Header as='h3' content='Administrador de Mensajes' style={style.h3} textAlign='center' />
        <Container text>
        <Segment.Group>
            <Segment>Mensaje1</Segment>
            <Segment>Content</Segment>
            <Segment>Content</Segment>
        </Segment.Group>
                
        <Link to={'/mensajes/'}>
            <Button primary>Escribir Mensajes</Button>
        </Link>
        
        </Container>
       </>
   )
}

export default Admin;
