import React from 'react';
import { Message } from 'semantic-ui-react';

const MsgExitoso = () => {
  return (
    <Message color='green'>
      <p>
        Tu mensaje ha sido enviado con <b>éxito</b>
      </p>
    </Message>
  )

}

export default MsgExitoso;